#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include "diagramme.h"
#include "points.h"
#include "listedespoints.h"
#include "parametredecalcul.h"

#include<vector>

using namespace std;

MenuG::MenuG(const string & titre)
{
	titre_ = titre;
}


void MenuG::afficherMenuPrincipal() //Menu Principal
{
    system("cls");
    for (int i=1; i<=80; i++)
    {
        cout << "-";
    }
    cout << "Les points saisis sont :" << endl;
            list1.afficher();      //Affichage des points saisis dans le menu principal
    for (int i=1; i<=80; i++)
    {
        cout << "-";
    }
    cout << "Les parametres de calcule sont :"<<endl;
        calcp1.afficher();  //Affichage des parametres de calcule
    for (int i=1; i<=80; i++)
    {
        cout << "-";
    }
    int size1;
    size1=list1.getsize();
    if (size1>=1)
    {
    poly.resize(size1);
    poly=calcp1.polynome(list1);
    calcp1.afficher_polynome(poly);  //Affichage des parametres de calcule
    }
    else
    {
    cout <<"Polynome par encore calcul� car il n'y a aucun point saisis"<<endl;
    }
    for (int i=1; i<=80; i++)
    {
        cout << "-";
    }
	int option;
	cout << " Choisissez votre option: " << endl;
	cout << "1 - Points" << endl;
	cout << "2 - Parametre de calcule" << endl;
	cout << "3 - Polynome d'interpolation" << endl;
	cout << "4 - Afficher courbe" << endl;
	cout << "5 - Quitter" << endl;
	cout << "Votre choix : ";
	cin >> option; //Affichage de toutes les options disponible dans le menu principal
	switch (option) // Switch permettant l'appel des differents menus ou fonctions du programme
	{
		case 1:
			afficherMenuPoint();
			break;
		case 2:
			afficherMenuParametre();
			break;
		case 3:
			afficherMenuPolynome();
			break;
        case 4:
			afficherCourbe();
			break;
        case 5:
			quitter();
			break;
		default:
			cout << "Choix incorrect!";
			break;
	}
    cout << endl << endl;
}

void MenuG::quitter() //Methode quitter permettant � l'utilisateur de valider son choix de fin de programme
{
   char reponse;
    cout << "Voulez-vous vraiment sortir de l'application (o/n) ?";
    cin >> reponse;
    if ((reponse == 'o') || (reponse == 'O'))
    {
        exit(0);
    }
}

void MenuG::afficherMenuPoint() //Methode qui affiche le menu point
{
    system("cls");
	int option;
	cout << "1 : Saisir des points" << endl;
	cout << "2 : Charger un fichier contenant des points" << endl;
	cout << "3 : Modifier un point" << endl;
	cout << "4 : Afficher tous les points" << endl;
	cout << "5 : Enregistrer les points dans un fichier" << endl;
	cout << "6 : Retour au menu pr�c�dent" << endl;
	cout << "Votre choix : ";   //Affiche des differents choix du menu point
	cin >> option;
	switch (option)
	{
		case 1: //Premier choix permettant de saisir des points � l'aide des classe listedesponts et points
			system("cls"); //Nettoyage de la console
            cout << "Combien de points voulez vous entrer " << endl;
            int nombredepoint;
            cin >> nombredepoint;
            for (int i = 0; i < nombredepoint; i++)
            {
                point *b = new point;
                b->saisir();
                list1.ajouter(b);

            }
			break;
		case 2: //Choix 2 permettant des charger des points enregistrer dans un fichier texte
			cout << " LE fichier ouvert est --valeur.txt-- et il contient la liste de point ci-dessous :" << endl;
            fichier1.open("valeur.txt");
            if (!fichier1.is_open()) cout << "Erreur d'ouverture" << endl;
            else
            {
            list1.charger(fichier1);
            }
            if (!fichier1.good()) cout << "Erreur de lecture " << endl;
            else cout << " Lecture termin�e " << endl;
            list1.afficher();
            cout << endl;
            system("pause");
            afficherMenuPoint();
		case 3: //Choix 3 permettent de modifier des points
		    system("cls");
		    list1.afficher(); //Affichage des points
            int num;
            cout << "Quel point voulez vous modifier ?"<<endl; //Saisis du point � modifier
            cin >>num;
			list1.setpoint(num); //Modification du point
			char reponse;
            cout << "Voulez-vous modifier un autre point (o/n) ?"<<endl; //Proposition de modification d'un autre point
            cin >> reponse;
            while (reponse == 'o') //Boucle permettant un nombre de modification de point infini
            {
            system("cls");
		    list1.afficher();
            cout << "Quel point voulez vous modifier ?"<<endl;
            cin >>num;
			list1.setpoint(num);
			cout << "Voulez-vous modifier un autre point (o/n) ?"<<endl;
            cin >> reponse;
            }
			break;
        case 4: // Choix 4 permettant l'affichage des points
			system("cls");
            cout << "Les points saisis sont :" << endl;
            list1.afficher();
            system("pause");
			break;
        case 5: //Choix 5 enregistrement des points dans un fichier txt
			//cout << "Votre lite de point est enregsitr� dans le fichier --valeur.txt--" << endl;
			fichierin.open("valeur.txt");
			if (!fichierin.is_open()) cout << "Erreur d'ouverture" << endl;
            else
            {
			list1.sauver(fichierin);
            }
            if (!fichierin.good()) cout << " Erreur d'�criture " << endl;
            else{ cout << "Enregistrement termin� !" << endl; }
            system("pause");
            cout << endl;
            break;
        case 6: // Choix 6 retour au menu principal
			afficherMenuPrincipal();
			break;
		default:
			cout << "Choix incorrect!";
			break;
	}
    cout << endl << endl;
    afficherMenuPoint();
}

void MenuG::afficherMenuParametre() //Methode affichage menu des parametre de calcul
{
    system("cls");
	int option;
    cout << "1 : Saisir l'intervalle de calcul" << endl;
	cout << "2 : Saisir le nombre de point interpole"<< endl;
	cout << "3 : Retour" << endl;
	cout << "Votre choix : ";
	cin >> option; // Affichage des differents choix du menu
	switch (option)
	{
		case 1: // Choix 1 saisie de l'intervalle de calcul
		    int borne1,borne2;
		    cout<<"Veuillez saisir la borne 1 : "<<endl;
		    cin >> borne1;
		    cout<<"Veuillez saisir la borne 2 : "<<endl;
		    cin >> borne2;
            calcp1.modifierIntervalle(borne1,borne2);
            system("pause");
			break;
		case 2: //Choix 2 saisis du nombre de point � calculer
            int nbpts;
		    cout<<"Veuillez saisir le nombre de points : "<<endl;
		    cin >> nbpts;
		    calcp1.modifierNombrePts(nbpts);
			break;
        case 3: // Choix 3 retour au menu principal
			afficherMenuPrincipal();
			break;
		default:
			cout << "Choix incorrect!";
			break;
	}
afficherMenuParametre();
}

void MenuG::afficherMenuPolynome() //Methode affichage du polynome
{
    system("cls");

	int option;
    cout << "1 : Afficher les points calcul�s" << endl;
	cout << "2 : Enregistrer dans un fichier" << endl;
	cout << "3 : Retour au menu pr�c�dent" << endl;
	cout << "Votre choix : ";
	cin >> option;
	switch (option)
	{
		case 1:
            list2=calcp1.calcul_points(poly,list1);
            list2.afficher();
            system("pause");
			break;
		case 2:
            fichierin2.open("valeurPoly.txt");
			if (!fichierin2.is_open()) cout << "Erreur d'ouverture" << endl;
            else
            {
			list2.sauver(fichierin2);
            }
            if (!fichierin2.good()) cout << " Erreur d'�criture " << endl;
            else{ cout << "Enregistrement termin� !" << endl; }
            system("pause");
            cout << endl;
			break;
        case 3:
			afficherMenuPrincipal();
			break;
		default:
			cout << "Choix incorrect!";
			break;
	}
    cout << endl << endl;
    afficherMenuPolynome();
}

void MenuG::afficherCourbe()
{
    system("cls");
    calcp1.tracer(poly);
    system("pause");
    afficherMenuPrincipal();
}
