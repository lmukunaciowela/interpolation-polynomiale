#ifndef _DIAGRAMME
#define _DIAGRAMME

#include <string>
#include <vector>
#include <iomanip>
#include <fstream>
#include <cstdlib>
#include "listedespoints.h"
#include "parametredecalcul.h"


using namespace std;

class MenuG
{
	string titre_; //titre du menu
    liste_de_points list1, list2; //liste de point du programme
    parametredecalcul calcp1; //Parametre de calcul pour le polynome
    ofstream fichierin, fichierin2;
	ifstream fichier1;
    vector<double> poly; //Tableau contenant les coefficiants du polynome

public:
	MenuG(const string & titre); //constructeur du menu
	void afficherMenuPrincipal();//affichge du menu principal
	void afficherMenuPoint(); //affichage du menu point
	void afficherMenuParametre();//affichage du menu parametre
	void afficherMenuPolynome();//affichage du menu polynome
	void afficherCourbe(); //affichage de la courbe
	void quitter(); //Quitter le programme
};

#endif

