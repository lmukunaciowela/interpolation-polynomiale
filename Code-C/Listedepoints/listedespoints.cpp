#include"listedespoints.h"
#include<iostream>
#include<fstream>

using namespace std;

/*liste_de_points::~liste_de_points()
{
	unsigned int i;
	for (i = 0; i < liste_des_points_.size(); i++)
	{
		delete liste_des_points_[i];
	}
}*/

void liste_de_points::afficher()
{
	for (unsigned int i = 0; i < liste_des_points_.size(); i++)
	{
		cout << "Point "<< i+1 << " : " ;
		liste_des_points_[i]->afficher();
	}
}


point liste_de_points::retournePoint(int pos)
{
		float x = liste_des_points_[pos]->retourneX();
		float y = liste_des_points_[pos]->retourneY();
		point a(x, y);
		return a;
}

void liste_de_points::ajouter(point *e)
{
	liste_des_points_.push_back(e);
}

void liste_de_points::setpoint(int pos)
{
	pos--;
	if (pos < 0)
	{
		cout << "Veuillez saisir le num�ro du point � modifier" << endl;
	}
	else
	{
		liste_des_points_[pos]->saisir();
		cout << " Modification prise en compte et vous avez saisis : " << endl;
		liste_des_points_[pos]->afficher();
	}
}
double liste_de_points::retourne_X(int pos)
{
	return liste_des_points_[pos]->retourneX();
}
double liste_de_points::retourne_Y(int pos)
{
	return liste_des_points_[pos]->retourneY();
}

int liste_de_points::getsize()
{
	return liste_des_points_.size();
}

void liste_de_points::sauver(ofstream &fichier)
{
	if (!fichier.is_open()) cout << " Erreur d'ouverture " << endl;
	else
	{
		int mult = liste_des_points_.size();
		fichier << mult << endl;
		for (unsigned int i = 0; i < liste_des_points_.size(); i++)
		{
			liste_des_points_[i]->sauver(fichier);
		}
	}
}

void liste_de_points::charger(ifstream &fichier)
{
	for (int i = 0; i < (int)liste_des_points_.size(); i++) delete liste_des_points_[i];
	liste_des_points_.clear();


	if (!fichier.is_open()) cout << " Erreur d'ouverture " << endl;
	else
	{
		int n;
		fichier >> n;
		fichier.ignore();
		for (int i = 0; i < n; i++)
		{
			point *m=new point;
			m->charger(fichier);
			ajouter(m);
		}
	}
}
