#ifndef _LISTE_DE_POINTS
#define _LISTE_DE_POINTS

#include<vector>
#include<fstream>
#include"points.h"
class point;
using namespace std;

class liste_de_points
{
	vector<point*> liste_des_points_;
public:
	//liste_de_points();// constructeur du vecteur liste des points
	//~liste_de_points(); // Destructeur
	void afficher(); // affiche les points pr�sent dans le vecteur point
	point retournePoint(int pos); // retourne un point
	void ajouter(point *e); //ajouter un nouveau point au vecteur
	void setpoint(int pos); // Permet de modifier un point selon sa position dans le vecteur point
	int getsize(); // Obtenir le nombre d'�l�ment
	void sauver(ofstream &fichier); // Sauvergarde un point
	void charger(ifstream &fichier);// Ouvrir un fichier
	double retourne_X(int pos); //retorune la valeur de x
	double retourne_Y(int pos); //retourne la valeur de y
};

#endif
