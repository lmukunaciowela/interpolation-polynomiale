#include"parametredecalcul.h"
#include<math.h>
#include<iostream>
#include <cstdlib>
#include"gnuplot.h"

using namespace std;

using namespace std;


parametredecalcul::parametredecalcul()
{
	borne1_ = 0;
	borne2_ = 3;
	nombre_ = 3;
}
void parametredecalcul::modifierIntervalle(double borne1, double borne2)
{
	borne1_ = borne1;
	borne2_ = borne2;
}
void parametredecalcul::modifierNombrePts(double nombre)
{
	nombre_ = nombre;
}

void parametredecalcul::afficher()
{
	cout << "Borne 1 :" <<borne1_<<endl;
	cout << "Borne 2 :" <<borne2_<<endl;
	cout << "Nombre de points :" <<nombre_<<endl;
}

void parametredecalcul::afficher_polynome(vector<double> polynome)
{
	int degre = polynome.size()-1;
	cout << "La fonction d'interpolation est la suivante" << endl;
	cout<<" f(x) = ";
		for (unsigned int i = 0; i < polynome.size()-1; i++)
		{
			cout<< polynome[i]<<"*x^"<<degre--<<"+";
		}
		degre = polynome.size() - 1;
		cout <<polynome[degre]<< endl;
}
vector <double> parametredecalcul::polynome(liste_de_points liste)
{
	int ligne = liste.getsize();
	int imax;
	double r,t;
	bool erreur=0;
	unsigned int degre = ligne-1;// je definis le degre de mon polynome
	vector<vector<double> > tab (ligne, vector<double>(ligne+1)); // Cre�e un tableau variable de 3 dimensions
	 vector<double> polynome(ligne);

	for (int i = 0; i < ligne; i++)
	{
		for (int j = 0; j < ligne+1; j++)
		{
			tab[i][j] = pow(liste.retourne_X(i),degre--);
		}
		tab[i][ligne] = liste.retourne_Y(i);
		degre = ligne - 1;
		//polynome[i] = liste.retourne_Y(i);
	}
	/// Elimination par  pivot de gauss
	for (int i = 0; i < degre; i++)
	{
		imax = i;
		for (int j = i + 1; j < ligne; j++)

			if (fabs(tab[j][i]) > fabs(tab[imax][i])) imax = j;
			if (tab[imax][i] == 0)
			{
				erreur = true;
				break;
			}

		for (int k = i; k < ligne ; k++) // Permutation des lignes
		{
			t = tab[i][k];
			tab[i][k] = tab[imax][k];
			tab[imax][k]=t;
		}
		for (int j = i+1; j < ligne; j++) // �limination
		{
			r = tab[j][i] / tab[i][i];
			for (int k = i; k < ligne + 1; k++)
			{
				tab[j][k] = tab[j][k] - tab[i][k] * r;
			}
		}
	}
	// phase de substitution,remplit le tableau X par les valeurs de la solution du systeme
	double z;
	for (int i = ligne - 1; i >= 0; i--)
	{
		z = 0;
		for (int j = i + 1; j < ligne; j++)
		{
			z = z + tab[i][j] * polynome[j];
		}
			polynome[i] = (tab[i][ligne] - z) / tab[i][i];

	}
	return polynome;
}
void parametredecalcul::tracer(vector<double> poly)
{
	Gnuplot gp;
	if (!gp.estOk())
	{
		cout << "Erreur : le lancement de gnuplot a echoue !" << endl;
		cout << "Verifier l'installation et verifier que la variable GNUPLOT est correctement initialisee avec le chemin vers le repertoire bin de gnuplot" << endl;
		abort();
	}

	bool erreur;

	erreur = gp.plot("points.dat");
	if (erreur)
	{
		cout << "Le fichier points.dat n'a pas ete trouve" << endl;
		abort();
	}

	vector<double> x(nombre_), y(nombre_);
	unsigned int degre = poly.size()-1;
	const int degrec = poly.size() - 1;
	for (int i = 0; i < nombre_; i++)
	{
		x[i] = borne1_ + 0.2*i;
		y[i] = poly[degre];
		for (int j = 0; j < degrec; j++)
		{
			y[i] = y[i]+pow(x[i], degre)*poly[j];
			degre--;
		}
		degre = poly.size()-1;
	}

	gp.plot(x, y, "f(x) :", "lines", true);

	system("pause");
	//cout << " Indisponible" << endl;
}
liste_de_points parametredecalcul::calcul_points(vector<double> poly, liste_de_points liste)
{
	vector<double> x(nombre_), y(nombre_);
	unsigned int degre = poly.size() - 1;
	const int degrec = poly.size() - 1;
	for (int i = 0; i < nombre_; i++)
	{
		x[i] = borne1_ + 0.2*i;
		y[i] = poly[degre];
		for (int j = 0; j < degrec; j++)
		{
			y[i] = y[i] + pow(x[i], degre)*poly[j];
			degre--;
		}
		degre = poly.size() - 1;
		double a = x[i];
		double b = y[i];
		point *m = new point(a, b);
		liste.ajouter(m);
	}
	return liste;
}
