#ifndef _PARAMETRE_DE_CALCUL
#define _PARAMETRE_DE_CALCUL
#include "listedespoints.h"
#include<vector>
class listedespoints;

class parametredecalcul
{
	double nombre_, borne1_, borne2_;
public:
	parametredecalcul(); //constructeur permettant l'initialisation de parametre de calcul
	void modifierNombrePts(double nombre); //Modifier le nombre de point des parametre de calcul
	void modifierIntervalle(double borne1,double borne2);//Modifier les bornes des parametre de calcul
	vector<double> polynome(liste_de_points liste);
	void afficher(); //Permet d'afficher les parametres de calcul
	void afficher_polynome(vector<double> polynome);//Permet d'afficher le polynome calcul�
	void tracer(vector<double> poly); //Permet de tracer le polynome
	liste_de_points calcul_points(vector<double> poly,liste_de_points liste); // Permet de calculer des points � l'aide du polynome calcul�
};

#endif
