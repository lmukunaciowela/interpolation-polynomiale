#ifndef _POINT
#define _POINT
#include<fstream>
using namespace std;

class point
{
	double x_, y_;
public:
	point(double x, double y); // constrcteur de point 
	point(); // Constructeur de point
	double retourneX() { return x_; }// retorune la valeur de x
	double retourneY() { return y_; } // retoruen la valeur de y
	void afficher(); // affiche le point 
	void saisir();// permet de saisir un point
	void sauver(ofstream &fichier);// sauvegarder un point
	void charger(ifstream &fichier);// ouvrir u point dans un fichier
};

#endif