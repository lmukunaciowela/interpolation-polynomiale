# Interpolation-Polynomiale
Exercice effectué dans le cadre scolaire avec un binôme, écrit en C/C++.

Dans un premier temps, le programme demande à l'utilisateur de saisir un certain nombre,qui seront enregistrés
dans un fichier texte, pour lequel il aimerait avoir la fonction qui passe par tout les points saisis.
Ensuite, il a la possibilité de modifier les points saisis, de definir l'intervalle de calcul,
le nombre de point à prendre en considération et il peut finalement tracer la courbe corespondante à la fonction recherchée.


 Exercise made in classes with a classmate, writing in C/C++
 
 First of all, the program asks the user to enter a certain number of point, that will be save on a text file,
 for which is looking for the function that includes the points entered.
 Then, he have the possibilty to edit the points entered, to define a competition of interval,
 the number of point to consider during the calculation and he can plot the result.
 
 
 Lionel MUKUNA CIOWELA
  
